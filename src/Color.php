<?php

namespace Pcaptcha;

class Color {

    /**
     * Red
     * @var integer
     */
    public $red;

    /**
     * Green
     * @var integer
     */
    public $green;

    /**
     * Blue
     * @var integer
     */
    public $blue;

    /**
     * Alpha
     * @var integer
     */
    public $alpha;

    /**
     * Construct the color object using html #RRGGBB format
     * @param $hash string
     */
    public function __construct($hash)
    {
        if (! is_string($hash))
            throw new \Exception('$hash is not a string');

        if ($hash[0] != '#')
            throw new \Exception('The first character is not #.');

        if (strlen($hash) != 7)
            throw new \Exception('The length of $hash is wrong.');

        // Red
        $this->red = hexdec(substr($hash, 1, 2));

        // Green
        $this->green = hexdec(substr($hash, 3, 2));

        // Blue
        $this->blue = hexdec(substr($hash, 5, 2));
    }

}
