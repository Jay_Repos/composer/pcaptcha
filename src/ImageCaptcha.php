<?php

namespace Pcaptcha;

class ImageCaptcha
{
    
    // ======= Properties =======
    /**
     * The width of the captcha image
     * @var int
     */
    public $image_width = 300;

    /**
     * The height of the captcha image
     * @var int
     */
    public $image_height = 100;

    /**
     * Font size is calculated by image height and this ratio.  Leave blank for
     * default ratio of 0.4.
     *
     * Valid range: 0.1 - 0.99.
     *
     * Depending on image_width, values > 0.6 are probably too large and
     * values < 0.3 are too small.
     *
     * @var float
     */
    public $font_ratio = 0.4;

    /**
     * The color of the captcha text
     * @var Securimage_Color|string
     */
    public $text_color     = '#707070';

    /**
     * The background color
     * @var string
     */
    public $bg_color = '#FFFFFF';

    /**
     * How many lines to draw over the captcha code to increase security
     * @var int
     */
    public $num_lines    = 3;

    /**
     * The level of noise (random dots) to place on the image, 0-10
     * @var int
     */
    public $noise_level  = 3;

    /**
     * The TTF font file to use to draw the captcha code.
     *
     * Leave blank for default font AHGBold.ttf
     *
     * @var string
     */
    public $ttf_dir = __DIR__ . '/../resources/fonts';

    /**
     * The code
     *
     * @var string
     */
    public $code = 'SAMPLE';

    /**
     * The GD image resource of the captcha image
     *
     * @var resource
     */
    protected $img;

    /**
     * GD color for text
     *
     * @var integer
     */
    protected $gd_textcolor;

    /**
     * GD color for background
     *
     * @var integer
     */
    protected $gd_bg_color;

    /**
     * TTF Files
     *
     * @var array
     */
    protected $ttf_files = [];

    /**
     * Return a random float between 0 and 0.9999
     *
     * @return float Random float between 0 and 0.9999
     */
    protected static function frand()
    {
        return 0.0001 * mt_rand(0,9999);
    }


    public function doImage() {
        $this->initGd();

        self::drawLines($this->img, $this->gd_textcolor, $this->num_lines);
        self::drawNoise($this->img, $this->gd_textcolor, $this->noise_level);
        self::drawWord($this->img, $this->gd_textcolor, $this->code, 0.5, $this->ttf_files);
        self::drawBackground($this->img, $this->gd_bg_color);
        ob_start();
        imagepng($this->img);
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    protected function initGd() {
        // Create a new GD resource
        $this->img = imageCreateTrueColor($this->image_width, $this->image_height);
        imageSaveAlpha($this->img, true);
        $txtColor = new Color($this->text_color);
        $bgColor = new Color($this->bg_color);
        $transparent = imageColorAllocateAlpha($this->img, 0, 0, 0, 127);
        imageAlphaBlending($this->img, false);
        imageFilledRectangle($this->img, 0, 0, $this->image_width, $this->image_height, $transparent);
        imageAlphaBlending($this->img, true);
        

        $this->gd_textcolor = imageColorAllocate($this->img, $txtColor->red, $txtColor->green, $txtColor->blue);
        $this->gd_bg_color = imageColorAllocate($this->img, $bgColor->red, $bgColor->green, $bgColor->blue);

        // Scan all fonts
        $this->ttf_files = [];

        $files = scandir($this->ttf_dir);
        foreach($files as $file) {
            
            if (strcasecmp(substr($file, -4), '.ttf') == 0 || strcasecmp(substr($file, -4), '.otf') == 0) {
                $this->ttf_files[] = rtrim($this->ttf_dir, '/') . '/' . $file;
            }
        }
    }

    // ======= Functions Applied to the image
    protected static function drawBackground($gd, $color) {
        $width = imagesx($gd);
        $height = imagesy($gd);

        $tmpgd = imageCreateTrueColor($width, $height);
        imageAlphaBlending($tmpgd, false);
        imageCopy($tmpgd, $gd, 0, 0, 0, 0, $width, $height);

        imageFilledRectangle($gd, 0, 0, $width, $height, $color);
        imageCopy($gd, $tmpgd, 0, 0, 0, 0, $width, $height);
    }

    protected static function drawWord($gd, $color, $code, $ratio = 0.4, $font_files = []) {
        $width = imagesx($gd);
        $height = imagesy($gd);
        $length = strlen($code);

        $font_size = $height * $ratio;
        $fonts = []; // Fonts for each character
        $angles = []; // Angle for each character
        $boxes = [];
        $dists = []; // Distance to the next character

        //$angle0 = mt_rand(10, 30); // The first angle
        //$angleN = mt_rand(-30, 10); // The second angle
        $angle0 = 0;
		$angleN = 0;
		if (mt_rand(0, 1)) {
            $angle0 = -$angle0;
            $angleN = -$angleN;
        }
        $angleStep = ($angleN - $angle0) / ($length);
        $angle = $angle0; // Current angle
        $totalTxtWid = 0; // Total width

        for ($i = 0; $i < $length; $i++) {
            $font = $font_files[mt_rand(0, count($font_files) - 1)];
            $box = imageFTBbox($font_size, $angle, $font, $code[$i]);
            $txtWid = $box[2] - $box[0];
            //$dist = $i == $length - 1 ? 0 : mt_rand($txtWid * -0.15, $txtWid * -0.1); // Distance to the next character
            $dist = 0;
			$totalTxtWid += $box[2] - $box[0] + $dist;

            if ($i == 0 || $i == $length - 1)
                $totalTxtWid += abs($box[6] - $box[0]);

            $fonts[$i] = $font;
            $boxes[$i] = $box;
            $dists[$i] = $dist;
            $angles[$i] = $angle;

            $angle += $angleStep;
        }

        // Start to draw the text
        $box = $boxes[0];
        $x = mt_rand(-min($box[0], $box[6]), ($width - $totalTxtWid) - min($box[0], $box[6]));
        $txtHeight = max($box[3], $box[1]) - min($box[7], $box[5]);
        $y = mt_rand(10, $height - $txtHeight);
        $y += -(min($box[7], $box[5]));
        //$step = mt_rand($height * -0.15, $height * -0.1); // Random baseline step for each character
        $step = 0;
		if (mt_rand(0, 1))
            $step = -$step;

        for ($i = 0; $i < $length; $i++) {
            $box = $boxes[$i];
            $angle = $angles[$i];
            $font = $fonts[$i];
            $dist = $dists[$i];

            // Check whether text will fit into the image
            if ($y + max($box[3], $box[1]) > $height) {
                $y = -(min($box[7], $box[5])); // put $y to the highest possible position
                $step = abs($step);
            } else if ($y + min($box[7], $box[5]) < 0) {
                $y = $height - max($box[3], $box[1]); // put $y to the lowest possible position
                $step = -abs($step);
            }

            imageFTText($gd, $font_size, $angle, $x, $y, -$color, $font, $code[$i]);
            $x += $box[2] - $box[0];
            $x += $dist;

            $y += $box[3] - $box[1];
            $y += $step;
        }
    }

    protected static function drawLines($gd, $color, $num_lines) {
        $width = imagesx($gd);
        $height = imagesy($gd);

        for ($line = 0; $line < $num_lines; ++ $line) {
            $x = $width * (1 + $line) / ($num_lines + 1);
            $x += (0.5 - self::frand()) * $width / $num_lines;
            $y = mt_rand($height * 0.1, $height * 0.9);

            $theta = (self::frand() - 0.5) * M_PI * 0.33;
            $w = $width;
            $len = mt_rand($w * 0.4, $w * 0.7);
            $lwid = mt_rand(0, 2);

            $k = self::frand() * 0.6 + 0.2;
            $k = $k * $k * 0.5;
            $phi = self::frand() * 6.28;
            $step = 0.5;
            $dx = $step * cos($theta);
            $dy = $step * sin($theta);
            $n = $len / $step;
            $amp = 1.5 * self::frand() / ($k + 5.0 / $len);
            $x0 = $x - 0.5 * $len * cos($theta);
            $y0 = $y - 0.5 * $len * sin($theta);

            $ldx = round(- $dy * $lwid);
            $ldy = round($dx * $lwid);

            for ($i = 0; $i < $n; ++ $i) {
                $x = $x0 + $i * $dx + $amp * $dy * sin($k * $i * $step + $phi);
                $y = $y0 + $i * $dy - $amp * $dx * sin($k * $i * $step + $phi);
                imageFilledRectangle($gd, $x, $y, $x + $lwid, $y + $lwid, $color);
            }
        }
    }

    protected static function drawNoise($gd, $color, $noise_level) {
        $width = imagesx($gd);
        $height = imagesy($gd);

        for ($x = 1; $x < $width; $x += 20) {
            for ($y = 1; $y < $height; $y += 20) {
                for ($i = 0; $i < $noise_level; ++$i) {
                    $x1 = mt_rand($x, $x + 20);
                    $y1 = mt_rand($y, $y + 20);
                    $size = mt_rand(1, 3);

                    imageFilledArc($gd, $x1, $y1, $size, $size, 0, mt_rand(180,360), $color, IMG_ARC_PIE);
                }
            }
        }
    }

}
