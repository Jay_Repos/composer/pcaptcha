<?php

require '../vendor/autoload.php';

use Pcaptcha\ImageCaptcha;

$captcha = new ImageCaptcha();
$img = $captcha->doImage();

header('Content-Type: image/png');
echo $img;
